#!/usr/bin/env python
# coding: utf-8

import click
import codecs
import multiprocessing
from lxml import etree
import pickle
import datetime

errors_file = open('errors-{}.pickled'.format(datetime.date.today().strftime('%d-%m-%Y')), 'wb')
errors = []

def get_dates(file, filename) -> ['article_id', 'ptype', 'pdate']:
    article_id = get_article_id(file, filename)
    pub_dates = file.findall('.//pub-date')
    dates = []
    for pub_date in pub_dates:
        if 'pub-type' in pub_date.attrib:
            pub_type = pub_date.attrib['pub-type']
        else:
            pub_type = 'NONE'
        ysearch = pub_date.find('.year')
        year = ysearch.text.strip() if ysearch is not None else '1900'

        msearch = pub_date.find('.month')
        month = msearch.text.strip() if msearch is not None else '01'

        dsearch = pub_date.find('.day')
        day =  dsearch.text.strip() if dsearch is not None else '01'
        if day:
            pdate = '{}-{}-{}'.format(year, month, day)
        else:
            pdate = '{}-{}-{}'.format(year, month, 1)
        dates += [article_id + [pub_type, pdate]]
    return dates

def get_contributions(file, filename) -> ['article_id', 'role', 'grid', 'isni', 'institution_name', 'name', 'surname']:
    # AUTHORS
    contributors = file.findall('.//contrib')
    contributors_list = []
    article_id = get_article_id(file, filename)
    nd = {}

    # AFFILIATIONS
    affiliations = {}
    institutions = {}
    affs = file.findall('.//aff')
    
    for aff in affs:
        addr_line = aff.find('addr-line')
        if 'id' not in aff.attrib:
            pass
        else:    
            institutions[aff.attrib['id']] = get_institution(filename, aff)
            has_label = False
            if aff.find('label') is not None:
                has_label = True

            aff_address_parts = [a for a in aff.xpath('text()') if a != '\n']
            if has_label:
                aff_address_parts = aff_address_parts[1:]
            affiliations[aff.attrib['id']] = ' '.join(aff_address_parts)
 
    for contributor in contributors:
        name = contributor.find('./name/given-names')
        name_text = name.text if name is not None else ''
        surname = contributor.find('./name/surname')
        surname_text = surname.text if surname is not None else ''

        roles = contributor.findall('role')
        cont = {}
        cont['type'] = contributor.attrib.get('contrib-type', 'NONE')
    
        # ROLES
        cont['roles'] = []
        for role in roles:
            cont['roles'] += [role.text]
        
        # AFFILIATIONS
        cont_affs = contributor.findall('.//xref[@ref-type="aff"]')
        cont['affiliations_ids'] = []
        try:
            for cont_aff in cont_affs:
                cont['affiliations_ids'] += [cont_aff.attrib.get('rid', 'NONE')]
        except:
            errors += [{'error': 'gettting affiliation id', 'params': [file]}]            
        
        for aff in cont['affiliations_ids']:
            contributors_list += [article_id + [ cont['type']] + institutions.get(aff, ["","",""]) + [ name_text, surname_text]]

    return contributors_list

def get_institution(filename, affiliation):
    institution_wrap = affiliation.find('.//institution-wrap')
    if institution_wrap is not None:
        inst_ids = {inst_id.attrib.get('institution-id-type', 'NONE'): inst_id.text for inst_id in institution_wrap.findall('./institution-id')}
        institution = []
        for inst in institution_wrap.findall('./institution'):
            if inst.text:
                institution += [inst.text.replace('\n', ' ')]

        return [inst_ids.get('GRID', 'NONE'), inst_ids.get('ISNI', 'NONE'), ' | '.join(institution)]
    else:
        inst_ids = {}
        aff_address_parts = [a.replace('\n', ' ') for a in affiliation.xpath('text()') if a != '\n']
        institution = ' '.join(aff_address_parts)
        return ['NONE', 'NONE', institution]

def get_authors_names(file, filename) -> ['name', 'surname']:
    # AUTHORS
    contributors = file.findall('.//contrib')
    contributors_list = []
    for contributor in contributors:
        name = contributor.find('./name/given-names')
        name_text = name.text if name is not None else ''
        surname = contributor.find('./name/surname')
        surname_text = surname.text if surname is not None else ''
        contributors_list += [[name_text, surname_text]]
    return contributors_list

def get_doi(file, filename) -> ['doi']:
    doi_search = file.find('.//article-id[@pub-id-type="doi"]')
    if doi_search:
        doi = doi_search.text
    else:
        doi = ''
    return [doi]

def get_coi(file, filename) -> ['coi_type', 'coi_text']:
    # COI (NEW)
    e = file.findall('.//fn[@fn-type="COI-statement"]')
    coi_type = 'NONE'
    coi = etree.tostring(e[0], method='text', encoding='unicode').replace('\n', ' ') if len(e) > 0 else 'NONE'
    
    # COI (OLD)
    if coi == 'NONE':
        e = file.find('conflict')
        if e:
            coi = etree.tostring(e[0], method='text', encoding='unicode').replace('\n', ' ') if len(e) > 0 else 'NONE'
            coi = coi.replace('"', '""')
            if coi != 'NONE':
                coi_type = 'OLD'
    else:
        coi_type = 'NEW'
    return [coi_type, coi]

def get_fund_text(article, filename) -> ['fund_text']:
    fund = 'NONE'
    # FUND
    ef =  article.findall('.//funding-statement')
    if len(ef) > 0:
        fund = etree.tostring(ef[0], method='text', encoding='unicode') if len(ef) > 0 else 'NONE' 
        fund = fund.replace('"', '""')
    return [fund.replace('\n', '')]

def get_journal_id(article, filename) -> ['journal_id']:
    return [filename.split('/')[-2]]
 
def get_title_abstract(article, filename) -> ['title', 'abstract']:
    # TITLE
    title = ''
    et = article.findall('.//article-title')
    if len(et) > 0:
        title = etree.tostring(et[0], method='text', encoding='unicode')
        title = title.replace('"', '""').replace('\n', '')
    
    # ABSTRACT
    ea = article.findall('.//abstract')
    if ea:
        abstract = etree.tostring(ea[0], method='text', encoding='unicode')
        abstract = abstract.replace('"', '""').replace('\n', '')
    else:
        abstract = 'NONE'

    return [title.replace('\n', ''), abstract.replace('\n', '')]

def get_article_id(article, filename) -> ["article_id"]:
    filename = filename.strip()
    pmc_code = filename.strip().split('/')[-1].split('.')[-2][3:]
    return [pmc_code]


def get_institution_id(article, filename) -> ['id-type', 'id']:
    article_id = get_article_id(article, filename)
    ids = article.findall('.//aff/*/institution-id')
    ids_cols = []
    if ids:
        for i in ids:
            ids_cols += [article_id + [i.attrib.get('institution-id-type', 'NONE'), i.text]]
        return ids_cols
    else:
        return [article_id + ["",""]]


def create_csv(col_functions, file_part, csv_filename, journal_files, rows=False):
    csv_file = open(csv_filename, 'w')
 
    csv = ''
    if file_part == 0:
        header = '\t'.join(['"{}"'.format(col) for func in col_functions for col in func.__annotations__['return']])
        csv += header + '\n'

    c = 0
    errors = []
    for i, file in enumerate(journal_files):
        try:
            filename = file.strip()
            pfile = etree.fromstring(codecs.open(filename, 'r', encoding='utf8').read())
            c += 1
            # pfile: Parsed XML file
            row = []
            for cf in col_functions:
                if not rows:
                    row += map(lambda s: '"{}"'.format(s), cf(pfile, filename))
                else:
                    for r in cf(pfile, filename):
                        row = map(lambda s: '"{}"'.format(s), r)
                        csv += '\t'.join(row) + '\n' 
    
            if not rows: 
                csv += '\t'.join(row) + '\n'                
    
            if c == 5000:
                csv_file.write(csv)
                csv_file.flush()
                c = 0
                csv = ''
        except:
            filename = file.strip()
            errors += [(csv_filename, file_part, filename)]
            print(errors[-1])
            continue
    
    pickle.dump(errors, errors_file)
    errors_file.flush()
    csv_file.write(csv)
    csv_file.flush()
    csv_file.close()

@click.command()
@click.option('--metadata',
              default='ardic')
@click.option('--batches',
              default=2,
              help='The person to greet.')
@click.argument('articles_list')
def run_parallel_csv_creator(batches, metadata, articles_list):
    journal_files = open(articles_list).readlines()
    jf_len = len(journal_files)//batches
    jobs = [] 
    
    for i in range(2):
        # Extract articles title, DOI, abstract and journal
        if 'a' in metadata:
            col_functions = [get_article_id, get_doi, get_fund_text, get_title_abstract, get_journal_id]
            p = multiprocessing.Process(target=create_csv, args=(col_functions, i, 'ARTICLES-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
            jobs.append(p)
            p.start()


        # Extract contributors and institutions from each article
        if 'r' in metadata:
            col_functions = [get_contributions]
            p = multiprocessing.Process(target=create_csv, args=(col_functions, i, 'CONTRIBUTIONS-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len], True))
            jobs.append(p)
            p.start()
    
        # Extract article publication dates
        if 'd' in metadata:
            col_functions = [get_dates]
            p = multiprocessing.Process(target=create_csv, args=(col_functions, i, 'DATES-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len], True))
            jobs.append(p)
            p.start()

        # Extract institution information from articles researchers
        if 'i' in metadata:
            col_functions = [get_institution_id]
            p = multiprocessing.Process(target=create_csv, args=(col_functions, i, 'INSTITUTION-ID-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len], True))
            jobs.append(p)
            p.start()

        # Extract COI from articles
        if 'c' in metadata:
            col_functions = [get_coi]
            p = multiprocessing.Process(target=create_csv, args=(col_functions, i, 'COI-{}'.format(i), journal_files[i*jf_len: (i+1)*jf_len]))
            jobs.append(p)
            p.start()

if __name__ == '__main__':
    run_parallel_csv_creator()
