\set db_user `echo $DB_USER`
\set db_anon `echo $DB_ANON_ROLE`
\set db_name `echo $DB_NAME`
\set db_schema `echo $DB_SCHEMA`
\set authenticator_pass `echo $DB_PASS`

drop role if exists :db_user;
create role :db_user with login password :'authenticator_pass';

create role :db_anon;
grant :db_anon to :db_user;

\set jwt_secret `echo $JWT_SECRET`
\set quoted_jwt_secret '\'' :jwt_secret '\''

\echo :quoted_jwt_secret

alter database :db_name set "app.jwt_secret" to :quoted_jwt_secret;
alter database :db_name set "app.debug" to 'f';

create extension if not exists plpython3u;
create extension if not exists pgcrypto;
create extension if not exists ltree;

\ir ./libs/pgjwt/schema.sql

-- create extension if not exists pgjwt;

create schema if not exists fg;
create schema if not exists fg_secure;

-- JOURNAL

create table if not exists fg.journal (
  id text primary key unique,
  name text
);

-- ARTICLE
create table if not exists fg.article (
    id         bigint     primary key,
    doi        text,
    title      text,
    title_search tsvector,
    abstract   text,
    abstract_search tsvector,
    filename   text unique,
    journal_id text,
    fund_text  text,
    metadata   jsonb         default '{}',
    authors    jsonb
);

create index idx_title on fg.article using gin(title_search);
create index idx_abstract on fg.article using gin(abstract_search);

create table if not exists fg.pub_date (
    id bigserial primary key,
    pdate date,
    ptype text,
    article_id bigint references fg.article (id)
);

create table if not exists fg.category (
    id bigserial primary key,
    ctype text,
    category ltree,
    article_id bigint references fg.article (id)
);

-- CONTRIBUTOR

create table if not exists fg.contributor (
  id           bigserial     primary key,
  name         text,
  surname      text,
  search_name  tsvector
);

create index searchname_idx on fg.contributor using gin (search_name);

create table if not exists fg.fund_institution (
   id                   bigserial      primary key,
   institution_id       text,
   institution_id_type  text
);

-- FUND

create table if not exists fg.fund (
   id                    bigserial     primary key,
   fund_institution_id   bigint not null references fg.fund_institution (id),
   fund_id               text
);

-- FUND RECIPIENT

create table if not exists fg.fund_recipient (
   id               bigserial primary key,
   name             text,
   surname          text,
   contributor_id   bigint references fg.contributor (id)
);

-- INSTITUTION

create table if not exists fg.research_institution (
  id              bigserial unique primary key,
  name            text,
  uid             jsonb,
  country         text
);

-- COI

create table if not exists fg.coi (
    id                   bigserial      primary key,
    coi_text             text,
    coi_type             text default 'DEFAULT',
    article_id bigint references fg.article (id)
);

-- COI ANNOTATION

create table if not exists fg.coi_annotation (
    id                   bigserial      primary key,
    atext                text
);

-- COI TAGS

create table if not exists fg.coi_tags (
    id                   bigserial      primary key,
    coi_id               bigint references fg.coi (id),
    tag                  text
);

-- CONTRIBUTION

create table if not exists fg.contribution (
  id               bigserial     primary key,
  type             text,
  role 		   text,
  article_id	   bigserial not null references fg.article (id),
  contributor_id   bigserial not null references fg.contributor (id),
  affiliation_id   bigserial references fg.research_institution (id)
);

alter table fg.contribution alter affiliation_id drop not null;

grant usage on schema fg to fg_authenticator;
grant all on all tables in schema fg to fg_authenticator;
grant usage, select on all sequences in schema fg to fg_authenticator;

