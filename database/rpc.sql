create or replace function fg.search_articles(date_order text, search_title text, search_abstract text)
  returns json
  language plpython3u
as $$
    import json

    params = []
    ptypes = []

    sql_query = """
      select doi, title, abstract, pdate, coi_text, fund_text, json_agg(json_build_object('author', surname || ', ' || name, 'author_id', fg.contributor.id))::jsonb from fg.article
        join fg.contribution on fg.contribution.article_id = fg.article.id
	join fg.contributor on fg.contribution.contributor_id = fg.contributor.id
        join fg.coi on fg.coi.article_id = fg.article.id
        join fg.pub_date on fg.pub_date.article_id = fg.article.id where fg.pub_date.ptype = 'epub'
	"""

    if search_title != '':
      sql_query += " and fg.article.title_search @@ to_tsquery($1)"
      params += ['%{}%'.format(search_title)]
      ptypes += ['text']

    if search_abstract:
      sql_query += " and fg.article.abstract_search @@ to_tsquery(${})".format(len(params) + 1)
      params += ['%{}%'.format(search_abstract)]
      ptypes += ['text']

    # sql_query += " order by fg.pub_date.pdate {} limit ${};".format(date_order, len(params) + 1)
    # params += [10]
    # ptypes += ["int"]

    sql_query += "group by doi, title, abstract, pdate, coi_text, fund_text"

    plpy.info(sql_query, params)

    query = plpy.prepare(sql_query, ptypes)
    result = plpy.execute(query, params)

    rows = []
    for r in result:
      rows += [r]

    return json.dumps(rows)
$$;

create or replace function fg.articles_with_authors(page_size text, page text)
  returns json
  language plpython3u
as $$
    import json

    params = []
    ptypes = []

    sql_query = """
      select doi, title, abstract, pdate, coi_text, fund_text, json_agg(json_build_object('author', surname || ', ' || name, 'author_id', fg.contributor.id))::jsonb from fg.article
        join fg.contribution on fg.contribution.article_id = fg.article.id
	join fg.contributor on fg.contribution.contributor_id = fg.contributor.id
        join fg.coi on fg.coi.article_id = fg.article.id
        join fg.pub_date on fg.pub_date.article_id = fg.article.id where fg.pub_date.ptype = 'epub'
	"""

    # sql_query += " order by fg.pub_date.pdate {} limit ${};".format(date_order, len(params) + 1)
    # params += [10]
    # ptypes += ["int"]

    sql_query += "group by doi, title, abstract, pdate, coi_text, fund_text limit $1 offset $2"
    params += [page_size, page]
    ptypes += ["int", "int"]

    plpy.info(sql_query, params)

    query = plpy.prepare(sql_query, ptypes)
    result = plpy.execute(query, params)

    rows = []
    for r in result:
      rows += [r]

    return json.dumps(rows)
$$;

create or replace function fg.search_coi (coi_text_search text, without_empty boolean default false, lim int default 20, pg int default 0)
  returns jsonb
  language plpython3u
as $$
    import json

    if coi_text_search != '':
      if not without_empty:
        sql_query = '''select count(fg.coi.article_id) from fg.coi where search_coi_text @@ to_tsquery('english', $1);'''
        query = plpy.prepare(sql_query, ["text"])
        result = plpy.execute(query, [coi_text_search])
      else:
        sql_query = '''select count(fg.coi.article_id) from fg.coi where search_coi_text @@ to_tsquery('english', $1) and coi_text != 'NONE';'''
        query = plpy.prepare(sql_query, ["text"])
        result = plpy.execute(query, [coi_text_search])
    else:
      if not without_empty:
        sql_query = '''select count(fg.coi.article_id) from fg.coi'''
      else:
        sql_query = '''select count(fg.coi.article_id) from fg.coi where coi_text != 'NONE';'''

      query = plpy.prepare(sql_query)
      result = plpy.execute(query)

    numResults = result[0]['count']
    pages = result[0]['count']//lim

    get_coi_query = '''
      select fg.article.title,
             fg.article.doi,
	     fg.article.journal_id,
             fg.coi.coi_text,
	     dt.pubdate, 
             json_agg(json_build_object('author', ctb.surname || ', ' || ctb.name, 'author_id', ctb.id))::json as authors
	from fg.coi 
	  join fg.article 
	    on fg.article.id = fg.coi.article_id 
	  join 
	    (select article_id, min(fg.pub_date.pdate) as pubdate from fg.pub_date group by article_id) dt on fg.article.id = dt.article_id 
	  join (select distinct fg.contributor.id, name, surname, article_id from fg.contributor join fg.contribution on fg.contribution.contributor_id = fg.contributor.id) ctb on ctb.article_id = fg.article.id 
	  '''

    has_search_text = False
    plus = 0
    if coi_text_search != '':
      get_coi_query += ''' where search_coi_text @@ to_tsquery('english', $1)''' 
      has_search_text = True
      plus = 1

    if without_empty:
      get_coi_query += ''' and coi_text != 'NONE' '''

    get_coi_query += ''' group by fg.article.id, fg.article.journal_id, fg.article.doi, fg.article.title, fg.coi.coi_text,pubdate 	order by dt.pubdate desc 
       limit ${} offset ${};'''.format(plus + 1, plus + 2)

    if has_search_text:
      query = plpy.prepare(get_coi_query, ["text", "int", "int"])
      result = plpy.execute(query, [coi_text_search, lim, pg])

    else:
      query = plpy.prepare(get_coi_query, ["int", "int"])
      result = plpy.execute(query, [lim, pg])

    results = list(result)
    for r in results:
      r['authors'] = json.loads(r['authors'])
    return json.dumps({'pages': pages, 'num_results': numResults, 'results': results});
$$;

create or replace function fg.author_coi(author_id int)
  returns setof json 
  language plpgsql
as $$
  begin
  return query 
	select json_build_object('id', fg.coi.id, 'count', count(coi_text), 'text', coi_text, 'type', coi_type) from fg.coi left join fg.article on fg.article.id = fg.coi.article_id left join fg.contribution on fg.contribution.article_id = fg.article.id where fg.contribution.contributor_id = author_id group by coi.id,coi_text,coi_type ;
  end;
$$;

create or replace function fg.author_articles(author_id int)
  returns json
  language plpython3u
as $$
    import json
    sql_query = """
      select fg.article.id as article_id, doi, title, abstract, pdate, coi_text, fund_text, journal_id from fg.article
        join fg.contribution on fg.contribution.article_id = fg.article.id
	join fg.contributor on fg.contribution.contributor_id = fg.contributor.id
        join fg.coi on fg.coi.article_id = fg.article.id
        join 
	    (select article_id, min(fg.pub_date.pdate) as pdate from fg.pub_date group by article_id) dt on fg.article.id = dt.article_id
        where fg.contributor.id = $1
	"""
    sql_query += "group by fg.article.id, doi, title, abstract, pdate, coi_text, fund_text, journal_id"

    query = plpy.prepare(sql_query, ["int"])
    result = plpy.execute(query, [author_id])

    json_result = {'articles': []}

    for r in result:

      sql_query = '''select json_agg(json_build_object('author', surname || ', ' || name, 'author_id', fg.contributor.id))::jsonb as authors
	from fg.contributor where id in (select contributor_id from fg.contribution where article_id = $1)
      '''
      query = plpy.prepare(sql_query, ["int"])
      result = plpy.execute(query, [r['article_id']])
      r['authors'] = json.loads(result[0]['authors'])

      json_result['articles'] += [r]

    sql_author_query = 'select name,surname from fg.contributor where id = $1'
    query = plpy.prepare(sql_author_query, ["int"])
    result = plpy.execute(query, [author_id])

    json_result['author'] = {'name': result[0]['name'], 'surname': result[0]['surname']}

    return json.dumps(json_result)
$$;
