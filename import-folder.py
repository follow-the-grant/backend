#!/usr/bin/env python
# coding: utf-8

import os
# import pandas as pd
import codecs
import json
# import glob
# import xmltodict
from lxml import etree
import pickle
import psycopg2
import sys


errors_file = open('errors.pickled', 'wb')
folder = sys.argv[1]
journal_files = [os.path.join(d, f) for d, _, fnames in os.walk(folder) for f in fnames if f.endswith('xml')]


# d = pd.DataFrame(columns=['file', 'coi', 'fund'])

# Connection to the database
conn = psycopg2.connect("dbname=app user=superuser host=localhost port=5435 password=superuserpass")
cur = conn.cursor()

pub_types = set()
errors = []

for i, file in enumerate(journal_files):
    # pfile: Parsed XML file
    pfile = etree.fromstring(codecs.open(file, 'r', encoding='utf8').read())
    # Insert journal if first article
    if i == 0:
        try:
            journal_name = pfile.find('.//journal-title').text
            sql_check_journal = 'select id from fg.journal where name = %s'
            cur.execute(sql_check_journal, (journal_name,))
            result = cur.fetchall()
            print('JOURNAL ', journal_name)
            if len(result) > 0:
                journal_id = result[0]
            else:
                sql_insert_journal = 'insert into fg.journal (name) values (%s) returning id'
                cur.execute(sql_insert_journal, (journal_name,))
                conn.commit()
                journal_id = cur.fetchone()[0]
        except psycopg2.OperationalError as e:
            print(e)
            errors += [{'error': 'inserting journal', 'params': [journal_name]}]

    nd = {}

    # 1. Insert article

    # PMID
    try:
        doi = pfile.find('.//article-id[@pub-id-type="doi"]').text
    except Exception:
        errors += [{'error': 'looking for PMID', 'params': [file]}]
        continue

    # TITLE
    try:
        et = pfile.findall('.//article-title')
        title = etree.tostring(et[0], method='text', encoding='unicode')
    except Exception:
        errors += [{'error': 'article title', 'params': [file]}]

    # ABSTRACT
    ea = pfile.findall('.//abstract')
    if ea:
        abstract = etree.tostring(ea[0], method='text', encoding='unicode')
    else:
        abstract = 'NONE'

    # FUND
    ef = pfile.findall('.//funding-statement')
    fund = etree.tostring(ef[0], method='text', encoding='unicode') if len(ef) > 0 else 'NONE'

    try:
        sql_insert_article = 'insert into fg.article (doi, title, abstract, filename, fund_text) values (%s, %s, %s, %s, %s) on conflict do nothing returning id '   # noqa
        cur.execute(sql_insert_article, (doi, title, abstract, file, fund))
        conn.commit()
        res_fetch = cur.fetchone()
        if res_fetch:
            article_id = res_fetch[0]
        else:
            continue
    except psycopg2.OperationalError as e:
        print(e)
        errors += [{'error': 'inserting article', 'params': [title, abstract, file, fund]}]
        continue

    # 2. Insert publication date information
    # PUBLICATION DATES

    pub_dates = pfile.findall('.//pub-date')
    for pub_date in pub_dates:
        try:
            pub_type = pub_date.attrib['pub-type']
            year = pub_date.find('.year').text if pub_date.find('.year') else '1900'
            month = pub_date.find('.month').text if pub_date.find('.month') else '01'
            day = pub_date.find('.day') if pub_date.find('.day') else '01'
            if day:
                pdate = '{}-{}-{}'.format(year, month, day)
            else:
                pdate = '{}-{}-{}'.format(year, month, 1)
            try:
                sql_insert_date = 'insert into fg.pub_date (article_id, ptype, pdate) values (%s, %s, %s)'
                cur.execute(sql_insert_date, (article_id, pub_type, pdate))
            except psycopg2.OperationalError:
                errors += [{'error': 'inserting publication date', 'params': [article_id, pub_type, pdate]}]
                continue
        except BaseException:
            errors += [{'error': 'no pub type', 'params': [article_id, pub_dates]}]
            continue

    # 3. Insert COI

    # COI (NEW)
    e = pfile.findall('.//fn[@fn-type="COI-statement"]')
    coi = etree.tostring(e[0], method='text', encoding='unicode') if len(e) > 0 else 'NONE'

    # COI (OLD)
    if coi == 'NONE':
        e = pfile.find('conflict')
        if e:
            coi = etree.tostring(e[0], method='text', encoding='unicode') if len(e) > 0 else 'NONE'

    sql_insert_coi = 'insert into fg.coi (article_id, coi_text) values (%s, %s)'
    cur.execute(sql_insert_coi, (article_id, coi))

    # 4. Insert category
    # SUBJECT

    categories_dict = {}
    categories = pfile.find('.//article-categories')
    for category in categories.getchildren():
        get_children = True
        if 'subj-group-type' not in category.attrib:
            subj_group_type = 'NONE'
        else:
            subj_group_type = category.attrib['subj-group-type']

        category_path = []
        while get_children:
            category_level = category.find('./subject')
            if category_level is not None:
                category_path += [category_level.text]
            category_sublevel = category.find('./subj-group')
            if category_sublevel is not None:
                category = category_sublevel
            else:
                get_children = False
        categories_dict[subj_group_type] = category_path

    sql_insert_category = 'insert into fg.category (article_id, ctype, category) values (%s, %s, %s)'
    for ctype, category in categories_dict.items():
        category_path = '.'.join(
            [
                c.replace(
                    ' ',
                    '_').replace(
                    '(',
                    '').replace(
                    '-',
                    '_').replace(
                        "'",
                        '').replace(
                            ':',
                            '').replace(
                                ')',
                                '').replace(
                                    '/',
                    '__') for c in category])
        try:
            cur.execute(sql_insert_category, (article_id, subj_group_type, category_path))
            conn.commit()
        except BaseException:
            print('ERROR => {}'.format(category_path))
            continue

    conn.commit()
    # 5. Insert authors

    # AUTHORS
    contributors = pfile.findall('.//contrib')
    contributors_list = []
    for contributor in contributors:
        cont = {}
        name = contributor.find('./name/given-names')
        cont['name'] = name.text if name is not None else ''
        surname = contributor.find('./name/surname')
        cont['surname'] = surname.text if surname is not None else ''

        # Check with author exists

        sql_check_author = "select id from fg.contributor where name = %s and surname = %s"
        try:
            cur.execute(sql_check_author, (cont['name'], cont['surname']))
            cur_result = cur.fetchall()
            if len(cur_result) > 0:
                author_id = cur_result[0][0]
            else:
                # Insert new author
                # print("INS")
                sql_insert_author = 'insert into fg.contributor (name, surname) values (%s, %s) returning id'
                cur.execute(sql_insert_author, (cont['name'], cont['surname']))
                conn.commit()
                author_id = cur.fetchall()[0][0]
        except psycopg2.OperationalError:
            errors += [{'error': 'checking author', 'params': [name, surname]}]
            continue

        roles = contributor.findall('role')
        cont['type'] = contributor.attrib['contrib-type']
        cont['id'] = author_id

        # ROLES
        cont['roles'] = []
        for role in roles:
            cont['roles'] += [role.text]
        contributors_list += [cont]

        # AFFILIATIONS
        cont_affs = contributor.findall('.//xref[@ref-type="aff"]')
        cont['affiliations_ids'] = []
        try:
            for cont_aff in cont_affs:
                cont['affiliations_ids'] += [cont_aff.attrib['rid']]
        except BaseException:
            errors += [{'error': 'gettting affiliation id', 'params': [file]}]

    nd['contributors'] = contributors_list

    # AFFILIATIONS
    affiliations = {}

    affs = pfile.findall('.//aff')

    for aff in affs:
        addr_line = aff.find('addr-line')
        if 'id' not in aff.attrib:
            pass
            # print('PROBLEMS AT: ', i, file)
            # print(etree.tostring(aff))
        else:
            affiliations[aff.attrib['id']] = addr_line.text if addr_line is not None else 'NONE'

    affiliations_ids = {}
    for aff_id, address in affiliations.items():
        sql_check_institution = "select id from fg.research_institution where name = %s"
        try:
            cur.execute(sql_check_institution, (address,))
            res = cur.fetchall()
            if len(res) > 0:
                affiliations_ids[aff_id] = res[0]
            else:
                sql_insert_institution = 'insert into fg.research_institution (name) values (%s) returning id'
                cur.execute(sql_insert_institution, (address,))
                new_affiliation_id = cur.fetchall()[0][0]
                # print('INSERTED', aff_id)
                affiliations_ids[aff_id] = new_affiliation_id
        except psycopg2.OperationalError as e:
            print('ERROR AFF', e)
            continue

    # Insert contributions
    sql_insert_contribution = 'insert into fg.contribution (type, contributor_id, affiliation_id, role, article_id) values (%s, %s, %s, %s, %s)'  # noqa
    for contributor in contributors_list:
        if contributor['affiliations_ids']:
            for aff_id in contributor['affiliations_ids']:
                # print(">> {}".format(affiliations, aff_id))
                try:
                    cur.execute(sql_insert_contribution, (
                        contributor['type'],
                        contributor['id'],
                        affiliations_ids[aff_id],
                        contributor['roles'],
                        article_id
                    ))
                    conn.commit()
                except BaseException:
                    continue
        else:
            cur.execute(sql_insert_contribution, (
                contributor['type'],
                contributor['id'],
                None,
                contributor['roles'],
                article_id
            ))
            conn.commit()

    # Insert contributors reference
    sql_insert_contributors_reference = 'update fg.article set authors = %s where id = %s'
    authors = []
    for contributor in contributors_list:
        authors += [{'author': '{}, {}'.format(contributor['surname'],
                                               contributor['name']), 'author_id': contributor['id']}]
    cur.execute(sql_insert_contributors_reference, (json.dumps(authors), article_id))

#         # FUND

#         funding_group = pfile.findall('.//funding-group/award-group')
#         awards_list = []
#         for award in funding_group:
#             award_d = {}
#             institution = award.find('.//institution')
#             award_d['institution'] = institution.text if institution is not None else ''
#             institution_id = award.find('.//institution-id')
#             if institution_id is not None:
#                 award_d['institution_id'] = institution_id.text
#                 award_d['institution_id_type'] = institution_id.attrib['institution-id-type']
#             award_id = award.find('./award-id')
#             award_d['id'] = award_id.text if award_id is not None else 'NONE'
#             award_recipient = award.find('.//principal-award-recipient')
#             if award_recipient:
#                 try:
#                     award_recipient_name = award_recipient.find('.//given-names').text
#                     award_recipient_surname = award_recipient.find('.//surname').text
#                 except:
#                     award_recipient_name = award_recipient.find('.//string-name').text
#                     award_recipient_surname = ''
#             else:
#                 award_recipient_name = 'NONE'
#                 award_recipient_surname = 'NONE'
#             award_d['recipient'] = [award_recipient_name, award_recipient_surname]
#             awards_list += [award_d]

#         nd['awards'] = awards_list
#         # financial-disclosure
#         financial_disclosure = pfile.find('.//financial-disclosure')

#         d = d.append(nd, ignore_index=True)

pickle.dump(errors, errors_file)
errors_file.flush()
